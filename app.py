from Football import Football
from Weather import Weather

football_filename  = "football.dat"
football_col_0 = 6
football_col_1 = 8
football_row = 1

football = Football(football_filename, football_col_0, football_col_1, football_row)
print(football.calculate(football.extract()))

weather_filename = "weather.dat"
weather_col_0 = 1
weather_col_1 = 2
weather_row = 0
weather = Weather(weather_filename, weather_col_0, weather_col_1, weather_row)
print(football.calculate(weather.extract()))
