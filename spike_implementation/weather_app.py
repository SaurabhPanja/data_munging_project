with open('weather.dat') as weather:
    tmp_dict = {}
    for i in weather:
        i_split = i.split()
        if len(i_split) > 0:
            max_tmp = i_split[1].replace('*','')
            min_tmp = i_split[2].replace('*','')
            if max_tmp.isnumeric() and min_tmp.isnumeric():
                tmp_dict[i_split[0]] = float(max_tmp) - float(min_tmp)

tmp_val = 9999
day = ''
for days, diff_weather in tmp_dict.items():
    diff_weather = float(diff_weather)
    if diff_weather < tmp_val:
        tmp_val = diff_weather
        day = days

print(f"""Temperatur Diff :{tmp_val}, Day: {day}""")
