import re
import sys


class DataMunger():
    def __init__(self, filepath, col_0, col_1, row):
        self.filepath = filepath
        self.col_0 = col_0
        self.col_1 = col_1
        self.row = row
        self.file_dict = {}


    def extract(self):
        with open(self.filepath) as file:
            for row in file:
                row_split = row.split()
                if len(row_split) > 8:
                    val_0 = re.sub(r'\D', '', row_split[self.col_0])
                    val_1 = re.sub(r'\D', '', row_split[self.col_1])
                    if val_0.isnumeric() and val_1.isnumeric():
                        self.file_dict[row_split[self.row]] = abs(
                            float(val_0) - float(val_1))
        return self.file_dict
    
    def calculate(self,file_dict):
        tmp_val = sys.maxsize
        req_key = ''
        for key, value in file_dict.items():
            value = float(value)
            if value == None or value < tmp_val:
                tmp_val = value
                req_key = key
        return req_key

